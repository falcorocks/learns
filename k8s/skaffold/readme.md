# flask

Minimal example comprising:
* flask python app and Dockerfile
* k8s Deployment and Namespace manifests
* skaffold for development

## Development

```bash
skaffold dev --port-forward=pods
    (...)
    Watching for changes...
    Forwarding container flask-cc4967684-64rvp/flask to local port 5002.
    Port forwarding pod/flask-cc4967684-64rvp in namespace default, remote port 5000 -> http://127.0.0.1:5002
    (...)
curl http://127.0.0.1:5002
    Hello, World!%         
```
