# k8s ingress

Minimal example comprising:
* traefik/whoami deployment and service
* nginx k8s ingress on whoami.127.0.0.1.nip.io
* skaffold

## Usage

```bash
# deploys ngnix controller into the cluster
make nginx

# deploys deployment, service, ingress and namespace
skaffold dev
    (...)
curl http://whoami.127.0.0.1.nip.io
    Hostname: whoami-deployment-76d8dbf989-kbx45
    IP: 127.0.0.1
    IP: 10.42.0.151
    RemoteAddr: 10.42.0.135:41346
    GET /whoami HTTP/1.1
    Host: 127.0.0.1.nip.io
    User-Agent: curl/7.77.0
    Accept: */*
    Accept-Encoding: gzip
    X-Forwarded-For: 10.42.0.131
    X-Forwarded-Host: 127.0.0.1.nip.io
    X-Forwarded-Port: 80
    X-Forwarded-Proto: http
    X-Forwarded-Server: traefik-55fdc6d984-njgjv
    X-Real-Ip: 10.42.0.131
```
